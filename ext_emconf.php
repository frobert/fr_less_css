<?php

########################################################################
# Extension Manager/Repository config file for ext "fr_less_css".
#
# Auto generated 13-04-2012 21:24
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'LESS CSS',
	'description' => '{less} - The dynamic stylesheet language. Extension includes less.js file to the footer of the page.',
	'category' => 'fe',
	'shy' => 0,
	'version' => '1.3.0',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 0,
	'lockType' => '',
	'author' => 'Robert Ferencek <robert@frstudio.si>',
	'author_email' => 'robert@frstudio.si',
	'author_company' => 'FR STUDIO',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:9:{s:9:"ChangeLog";s:4:"7283";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"50d2";s:14:"ext_tables.php";s:4:"5c34";s:19:"doc/wizard_form.dat";s:4:"b589";s:20:"doc/wizard_form.html";s:4:"6825";s:22:"less/less-1.3.0.min.js";s:4:"3666";s:32:"static/fr_less_css/constants.txt";s:4:"d41d";s:28:"static/fr_less_css/setup.txt";s:4:"3452";}',
);

?>